local nakama = require("nakama")
local http_utils = {}

--[[ Possible status codes are:
    GRPC  -  HTTP    GRPC  -  HTTP
	1        408     9        400
	2        500     10       409
	3        400     11       400
	4        504     12       501
	5        404     13       500
	6        409     14       503
	7        403     15       500
	8        429     16       401                               
]]--
function http_utils.ok(ok_body)
	return nakama.json_encode({["result"] = ok_body})
end

function http_utils.error(error_str, error_code)
	error_code = error_code or 12
	
	error({error_str, error_code})
	return nil --tecnically it should never return
end

return http_utils