local nk = require("nakama")
local utils = require("utils")

local function disable_rpc(context, payload)
	nk.logger_warn(string.format("CALLED A DISABLED RPC: %q Payload %q", utils.to_string(context), utils.to_string(payload)))
	return nil
end

-- Funzioni di autenticazione
nk.register_req_before(disable_rpc, "AuthenticateCustom")
nk.register_req_before(disable_rpc, "AuthenticateDevice")
nk.register_req_before(disable_rpc, "AuthenticateEmail")
nk.register_req_before(disable_rpc, "AuthenticateFacebook")
nk.register_req_before(disable_rpc, "AuthenticateFacebookInstantGame")
nk.register_req_before(disable_rpc, "AuthenticateApple")
nk.register_req_before(disable_rpc, "AuthenticateGameCenter")
nk.register_req_before(disable_rpc, "AuthenticateGoogle")
nk.register_req_before(disable_rpc, "AuthenticateSteam")

nk.register_req_before(disable_rpc, "LinkCustom")
nk.register_req_before(disable_rpc, "LinkDevice")
nk.register_req_before(disable_rpc, "LinkEmail")
nk.register_req_before(disable_rpc, "LinkFacebook")
nk.register_req_before(disable_rpc, "LinkFacebookInstantGame")
nk.register_req_before(disable_rpc, "LinkApple")
nk.register_req_before(disable_rpc, "LinkGameCenter")
nk.register_req_before(disable_rpc, "LinkGoogle")
nk.register_req_before(disable_rpc, "LinkSteam")

nk.register_req_before(disable_rpc, "UnlinkCustom")
nk.register_req_before(disable_rpc, "UnlinkDevice")
nk.register_req_before(disable_rpc, "UnlinkEmail")
nk.register_req_before(disable_rpc, "UnlinkFacebook")
nk.register_req_before(disable_rpc, "UnlinkFacebookInstantGame")
nk.register_req_before(disable_rpc, "UnlinkApple")
nk.register_req_before(disable_rpc, "UnlinkGameCenter")
nk.register_req_before(disable_rpc, "UnlinkGoogle")
nk.register_req_before(disable_rpc, "UnlinkSteam")

--Funzioni generali
nk.register_req_before(disable_rpc, "SessionRefresh")
nk.register_req_before(disable_rpc, "SessionLogout")
nk.register_req_before(disable_rpc, "ImportFacebookFriends")
nk.register_req_before(disable_rpc, "ImportSteamFriends")
nk.register_req_before(disable_rpc, "ValidatePurchaseApple")
nk.register_req_before(disable_rpc, "ValidatePurchaseGoogle")
nk.register_req_before(disable_rpc, "ValidatePurchaseHuawei")
nk.register_req_before(disable_rpc, "GetAccount")