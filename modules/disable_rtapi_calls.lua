local nk = require("nakama")
local utils = require("utils")

local function disable_rtapi_call(context, payload)
	nk.logger_warn(string.format("CALLED A DISABLED RTAPI FUNCTION: %q Payload %q", utils.to_string(context), utils.to_string(payload)))
	return nil
end

nk.register_rt_before(disable_rtapi_call, "MatchCreate")
nk.register_rt_before(disable_rtapi_call, "MatchDataSend")