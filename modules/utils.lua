local nakama = require("nakama")
local http  = require("http_utils")
local utils =  {}

--Clean the payload removing unnecessary " added if you don't use unwrap
function utils.clean_payload(payload)
	payload = string.gsub(payload, '\"', '"')
	local success, payload = pcall(nakama.json_decode, payload)
	if not success then
		nakama.logger_warn(payload)--payload becames the error
		http.error("Not a valid JSON string. Error decoding", 3)
	end
	return payload
end

function table_print (tt, indent, done)
  done = done or {}
  indent = indent or 0
  if type(tt) == "table" then
    local sb = {}
    for key, value in pairs (tt) do
      table.insert(sb, string.rep (" ", indent)) -- indent it
      if type (value) == "table" and not done [value] then
        done [value] = true
        table.insert(sb, key .. "={");
        table.insert(sb, table_print (value, indent + 2, done))
        table.insert(sb, string.rep (" ", indent)) -- indent it
        table.insert(sb, "}, ");
      elseif "number" == type(key) then
        table.insert(sb, tostring(value))
      else
        table.insert(sb, string.format(
            '%s = "%s", ', tostring (key), tostring(value)))
      end
    end
    return table.concat(sb)
  else
    return tt
  end
end

function utils.to_string( tbl )
    if  "nil"       == type( tbl ) then
        return tostring(nil)
    elseif  "table" == type( tbl ) then
        return table_print(tbl)
    elseif  "string" == type( tbl ) then
        return tbl
    else
        return tostring(tbl)
    end
end

return utils	