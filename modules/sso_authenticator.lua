local nk    = require("nakama")
local utils = require("utils")
local http  = require("http_utils")

--Local cache to speed up function calls
local clean_payload = utils.clean_payload
local logger_warn   = nk.logger_warn
local http_error    = http.error

local errors = {
	["not_autz"] = "You are not authorized to call this RPC",
	["username"] = "The username is required",
	["customId"] = "The customId is required",
	["oCustId"]  = "The oldCustomId is required",
	["nCustId"]  = "The newCustomId is required",
	["vars"]     = "vars must be json object composed by strings",
	["expiry"]   = "expiry must be a number greater than or equal to 0",
	["auth"]     = "Failed to get user, maybe the username is already registered",
	["token"]    = "Failed to get generate the token, maybe \"vars\" is not an object of strings",
	["linkCust"] = "Failed to link a custom id to this user",
	["delAccnt"] = "Error while trying to delete account",
}

-- Controlla che l'ip da cui proviene la richiesta sia quello del sistema di autn
local function can_request(context, payload)
	local client_ip = context["client_ip"] or ""
	local sso_server_ip = context["env"]["sso_server_ip"]
	
	local authorized = sso_server_ip ~= nil and client_ip == sso_server_ip
	
	if not authorized then
		logger_warn(string.format("NOT AUTHORIZED Context: %q Payload %q", utils.to_string(context), utils.to_string(payload)))
		return http_error(errors["not_autz"], 7)
	end
	return authorized
end

-- RPC chiamata dal sistema di autn per generare un token di accesso per un determinato
-- username e customId
local function sso_authenticate(context, payload)
	can_request(context, payload) --check if the user is authorized 
	
	payload = clean_payload(payload)
	
	local username  = payload["username"]
	local custom_id = payload["customId"]
	local expiry    = payload["expiry"] or 0
	local vars      = payload["vars"] or nil
	
	if username == nil then
		logger_warn(errors["username"])
		return http_error(errors["username"], 3)
	end
	
	if custom_id == nil then
		logger_warn(errors["customId"])
		return http_error(errors["customId"], 3)
	end
	
	if type(expiry) ~= "number" or expiry < 0 then
		logger_warn(errors["expiry"])
		return http_error(errors["expiry"], 3)
	end
	
	if vars ~= nil and type(vars) ~= "table" then
		logger_warn(errors["vars"])
		return http_error(errors["vars"], 3)
	end
	
	local success, user_id, username, created = pcall(nk.authenticate_custom, custom_id, username, true)
	if not success or (not created and username == "") then
		logger_warn(user_id) --user_id becames the error
		return http_error(errors["auth"], 5)
	end
	
	local success, token, expiry = pcall(nk.authenticate_token_generate, user_id, username, expiry, vars)
	if not success then
		logger_warn(token) --token becames the error
		return http_error(errors["token"], 3)
	end
	
	nk.logger_info(string.format("Access token for %s created", username))
	return http.ok({
		["created"] = created, 
		["token"] = token, 
		["expiry"] = expiry
	})
end
nk.register_rpc(sso_authenticate, "sso.authenticate")


-- RPC chiamata dal sistema di autn per aggiornare il customId di un utente
local function sso_update_custom_id(context, payload)
	can_request(context, payload) --check if the user is authorized 
	
	payload = clean_payload(payload)
	
	local username      = payload["username"]
	local old_custom_id = payload["oldCustomId"]
	local new_custom_id = payload["newCustomId"]
	
	if username == nil then
		logger_warn(errors["username"])
		return http_error(errors["username"], 3)
	end
	
	if old_custom_id == nil then
		logger_warn(errors["oCustId"])
		return http_error(errors["oCustId"], 3)
	end
	
	if new_custom_id == nil then
		logger_warn(errors["nCustId"])
		return http_error(errors["nCustId"], 3)
	end
	
	local success, user_id, username, created = pcall(nk.authenticate_custom, old_custom_id, username, false)
	if not success or (not created and username == "") then
		logger_warn(user_id) --user_id becames the error
		return http_error(errors["auth"], 5)
	end
	
	local success, err = pcall(nk.link_custom, user_id, new_custom_id)
	if not success then
		logger_warn(err)
		return http_error(errors["linkCust"], 2)
	end
	
	nk.logger_info(string.format("CustomID for %s changed", username))
	return http.ok("ok")
end
nk.register_rpc(sso_update_custom_id, "sso.update_custom_id")


-- RPC chiamata dal sistema di autn per eliminare definitivamente un utente
local function sso_delete_account(context, payload)
	can_request(context, payload) --check if the user is authorized 
	
	payload = clean_payload(payload)
	
	local username  = payload["username"]
	local custom_id = payload["customId"]
	
	if username == nil then
		logger_warn(errors["username"])
		return http_error(errors["username"], 3)
	end
	
	if custom_id == nil then
		logger_warn(errors["customId"])
		return http_error(errors["customId"], 3)
	end
	
	local success, user_id, username, created = pcall(nk.authenticate_custom, custom_id, username, false)
	if not success or (not created and username == "") then
		logger_warn(user_id) --user_id becames the error
		return http_error(errors["auth"], 5)
	end
	
	local success, err = pcall(nk.account_delete_id, user_id, true) --record the deletion
	if not success then
		logger_warn(err)
		return http_error(errors["delAccnt"], 2)
	end
	
	nk.logger_info(string.format("The account for %s has been COMPLETELY deleted", username))
	return http.ok("ok")
end
nk.register_rpc(sso_delete_account, "sso.delete_account")